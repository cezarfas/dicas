\babel@toc {brazil}{}
\contentsline {section}{\numberline {1}Passos do livro \textit {Curso Intensivo de Python} de Eric Matthes}{1}{section.1}
\contentsline {subsection}{\numberline {1.1}Parte inicial de cria\IeC {\c c}\IeC {\~a}o do projeto}{1}{subsection.1.1}
\contentsline {subsection}{\numberline {1.2}Cria\IeC {\c c}\IeC {\~a}o de aplica\IeC {\c c}\IeC {\~a}o, modelo e superusu\IeC {\'a}rio}{2}{subsection.1.2}
\contentsline {section}{\numberline {2}Anota\IeC {\c c}\IeC {\~o}es do curso EAD de Gregory Pacheco}{2}{section.2}
\contentsline {subsection}{\numberline {2.1}Anota\IeC {\c c}\IeC {\~o}es gerais}{2}{subsection.2.1}
\contentsline {subsection}{\numberline {2.2}As etapas, da cria\IeC {\c c}\IeC {\~a}o do projeto ao \textit {deploy} do projeto}{3}{subsection.2.2}
\contentsline {subsubsection}{\numberline {2.2.1}Resumo de instala\IeC {\c c}\IeC {\~o}es}{4}{subsubsection.2.2.1}
\contentsline {subsection}{\numberline {2.3} Configurando o \textit {pgAdmin3} }{5}{subsection.2.3}
\contentsline {section}{\numberline {3}Anota\IeC {\c c}\IeC {\~o}es do curso de Git e Github da \textit {School of Net}}{6}{section.3}
